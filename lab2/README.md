# Lab 2: Deploy a Serverless Application

You've now deployed a serverless function that you can call on demand. But what
if you want to spin up an entire application instead of just a function?
This lab will show you how to do so.

## About the Application

The application we will use is a web frontend written with golang and webassembly.
In the interest of time, the files have been provided for you in the **app** directory of this repo.
You should not need to edit these files.

In this lab, you will:

* Create a GitLab personal access token.
* Configure GitLab CI/CD to use that token.
* Use GitLab CI/CD to build a container image for your application, leveraging Kaniko to build the container image inside Kubernetes.
* Deploy your serverless function from Lab 1 and use its output in your serverless application.

## Create Access Token

In order for your CI/CD pipeline to proceed, you will need to create a GitLab
personal access token. Personal access tokens are the preferred way for third
party applications and scripts to authenticate with the GitLab API, and in the
context of this lab will be used as a way to authenticate with the GitLab
container registry.

To create a personal access token, click on your user in the top right corner
of the GitLab UI, and then click **Settings**:

![get_access_token_1](images/get_access_token_1.png)

On the screen that follows, click **Access Tokens** in the left-hand sidebar:

![get_access_token_2](images/get_access_token_2.png)

Name your token anything you want (suggested to relate it to the use of the token so you know which token is for what). Leave the expiration date unset. And ensure that all **five** check boxes are checked. Then click the green **Create Personal Access Token** button:

![get_access_token_3](images/get_access_token_3.png)

The page will refresh, and your new personal access token will be displayed at the **top** of the page.

**MAKE SURE YOU COPY THIS PERSONAL ACCESS TOKEN TO YOUR CLIPBOARD BEFORE YOU CLOSE THE TAB**.
Otherwise, you will need to generate a new token, as it will not be displayed
for you again:

![get_access_token_4](images/get_access_token_4.png)

## Add Access Token to CI/CD variables

Now that you have a token, you will need to add it to your CI/CD configuration
as an environment variable.

Go back to your project by either opening the **Projects** menu at the top of the GitLab UI, and then selecting your project (should be under **Frequently Visited**), or by clicking your browser back button until you get back to your project view (should be about three times).

![get_access_token_5](images/get_access_token_5.png)

To add your new token to your project variables, hover over **Settings** at the bottom of the left-hand sidebar (you might have to scroll down), then click on **CI/CD**.

![add_token_variable_1](images/add_token_variable_1.png)

Click the **Expand** button in the **Variables** section:

![add_token_variable_2](images/add_token_variable_2.png)

There should already be a few variables defined. Skip past those and in the box labeled **Input variable key**, type `CI_DEPLOY_PASSWORD`, and in the box labeled **Input variable value**, paste your personal access token from the previous section:

<!---
<kbd>
   <img alt="add_token_variable_3" src="images/add_token_variable_3.png">
</kbd>
--->

![add_token_variable_3](images/add_token_variable_3.png)


Once your variable data is filled in, click the **Masked** toggle switch to the right of your variable so that it is greyed out. This will make sure that the contents of your personal access token will not be dumped to CI output logs, but rather referenced as an environment variable.

When you are done, click the green **Save variables** button.

![add_token_variable_4](images/add_token_variable_4.png)

## Update CI/CD configuration (.gitlab-ci.yml)

We will need to update our CI/CD configuration (**.gitlab-ci.yml** file) to build and deploy our serverless application.

To edit this file, hover over **Repository** near the top of the left-hand sidebar, then click on **Files**.

![edit_ciyaml_1](images/edit_ciyaml_1.png)

As in Lab 1, we'll use GitLab's built-in Web IDE to navigate to and this time edit files. In the upper right corner of the **Files** page click on the Web IDE button:

![edit_ciyaml_2](images/edit_ciyaml_2.png)

Once in the Web IDE, on the file tree on the left side click to open the **.gitlab-ci.yml** file:

![edit_ciyaml_3](images/edit_ciyaml_3.png)

First, we'll need to edit the `stages` section to have two new stages:

```yaml
stages:
  - build-app
  - deploy-app
```

Now that we have added those stages, we will need to define what they do. Add the
following block after the `stages` block.
block:

```yaml
sample-app-build:
 stage: build-app
 environment: test
 image:
   name: gcr.io/kaniko-project/executor:debug-v0.6.0
   entrypoint: [""]
 before_script:
   - export DATE=$(date)
   - sed -i -e "s/REPLACE_WITH_DATE/$DATE/g" $CI_PROJECT_DIR/lab2/app/index.html
 script:
   - /busybox/echo "{\"auths\":{\"$CI_REGISTRY\":{\"username\":\"$CI_REGISTRY_USER\",\"password\":\"$CI_JOB_TOKEN\"}}}" > /kaniko/.docker/config.json
   - /kaniko/executor --context $CI_PROJECT_DIR --dockerfile $CI_PROJECT_DIR/lab2/app/Dockerfile --destination $CI_REGISTRY_IMAGE/sample-app:latest
```

Finally, add this block immediately after the `sample-app-build` block:

```yaml
deploy-hello-application:
 stage: deploy-app
 environment: test
 image: gcr.io/triggermesh/tm:latest
 before_script:
   - echo $TMCONFIG > tmconfig
   - tm --config ./tmconfig set registry-auth gitlab-registry --registry "$CI_REGISTRY" --username "$CI_REGISTRY_USER" --password "$CI_JOB_TOKEN" --push
   - tm --config ./tmconfig set registry-auth gitlab-registry --registry "$CI_REGISTRY" --username "$CI_DEPLOY_USER" --password "$CI_DEPLOY_PASSWORD" --pull
 script:
   - tm --config ./tmconfig deploy service sample-app -f "$CI_REGISTRY_IMAGE/sample-app:latest" --env SIMPLE_MSG="Hello GitLab $CI_JOB_URL" --wait; echo
```

Set the commit message to something like `Update .gitlab-ci.yaml`, then click
the green **Commit changes** button.

## Check the Pipeline

Click the **CI/CD** tab in the left-hand sidebar, then click on the most
recent pipeline. You will notice that there are now three stages, and your build
stage should be in **Running** state. You can click on the individual jobs to
get their output logs.

When the pipeline completes, the output of the `deploy-hello-application` job
will look like:

```shell
$ tm --config ./tmconfig deploy service sample-app -f "$CI_REGISTRY_IMAGE/sample-app:latest" --env SIMPLE_MSG="Hello GitLab $CI_JOB_URL" --wait
Creating sample-app function
Waiting for sample-app ready state
Service sample-app URL: http://sample-app-workshop-user1.k.triggermesh.io
```

The last line contains the URL for your application.

## Test the Application

Copy the URL from your job output log and paste it into a browser window. You
will be greeted with a message containing the job number used to deploy your
application. If you re-run the pipeline, you will see that this number will
increment!

## Continue to Lab 3

Please continue to [lab 3](../lab3/README.md)
